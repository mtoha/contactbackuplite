package com.essential.toha.utility;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import com.essential.toha.Async.AsyncSystemLogCreatePost;
import com.essential.toha.Async.IAsyncResponse;
import com.essential.toha.model.Contact;
import com.essential.toha.model.ExceptionLog;
import com.essential.toha.model.SystemLog;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.concurrent.ExecutionException;

/**
 * Created by TOHA on 7/20/2015.
 */
public class AndroidHelperUtility extends Activity {

    public static String STRING_LOG_TITLE_HTTP = "LOG_HTTP";
    public static boolean IS_DEBUG_MODE = true;

    public static String generateRefNumber() {
        Date now = new Date();
        String refNumber = new SimpleDateFormat("yyyyMMddHHmmss", Locale.ENGLISH).format(now);
        return refNumber;

    }

    public static String getBaseUrl(String path, boolean debugMode) {
        String server = "http://4cfc4f8c.ngrok.io";
        if (debugMode)
            return server + "/ContactServer/public/" + path + "?XDEBUG_SESSION_START=netbeans-xdebug";
        else
            return server + "/ContactServer/public/" + path;
    }

    public static void Notify(Activity activity, Context context, Class cClass, String title, String message) {
        NotificationManager nm = (NotificationManager) activity.getSystemService(Context.NOTIFICATION_SERVICE);
        Notification notification = new Notification(android.R.drawable.stat_notify_more, title, System.currentTimeMillis());

        CharSequence sTitle = title;
        CharSequence sDetails = message;

        Intent intent = new Intent(context, cClass);

        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);
        notification.setLatestEventInfo(context, sTitle, sDetails, pendingIntent);
        nm.notify(0, notification);
    }

    public static AlertDialog Alert(Context context, String sMessage, String sYesMessage, String sNoMessage, DialogInterface.OnClickListener diYes, DialogInterface.OnClickListener diNo) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(sMessage);
        builder.setCancelable(false);
        builder.setPositiveButton(sYesMessage, diYes);
        builder.setNegativeButton(sNoMessage, diNo);

        return builder.create();
    }

    public static int LogExceptionError(final ExceptionLog exceptionLog) {

        final HttpResponse[] response = new HttpResponse[1];
        new Thread(new Runnable() {
            public void run() {
                // Create a new HttpClient and Post Header
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(AndroidHelperUtility.getBaseUrl("exceptionlog/create", AndroidHelperUtility.IS_DEBUG_MODE));
                try {

                    Gson gson = new Gson();
                    String jsonString = gson.toJson(exceptionLog);

                    JSONObject jsonObject = new JSONObject();
                    jsonObject = new JSONObject(jsonString);

                    JSONArray jsonArray = new JSONArray();
                    jsonArray.put(jsonObject);

                    // Post the data:
                    httpPost.setHeader("json", jsonObject.toString());
                    httpPost.getParams().setParameter("jsonpost", jsonArray);

                    // Execute HTTP Post Request
                    System.out.print(jsonObject);
                    HttpResponse httpResponseponse = httpClient.execute(httpPost);
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }).start();

        if (response[0] != null) {
            return 1;
        } else {
            return 0;
        }
    }
    public static int ContactCreate(final Contact contact) {

        final HttpResponse[] response = new HttpResponse[1];
        new Thread(new Runnable() {
            public void run() {
                // Create a new HttpClient and Post Header
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(AndroidHelperUtility.getBaseUrl("contact/create", AndroidHelperUtility.IS_DEBUG_MODE));
                try {

                    Gson gson = new Gson();
                    String jsonString = gson.toJson(contact);

                    JSONObject jsonObject = new JSONObject();
                    jsonObject = new JSONObject(jsonString);

                    JSONArray jsonArray = new JSONArray();
                    jsonArray.put(jsonObject);

                    // Post the data:
                    httpPost.setHeader("json", jsonObject.toString());
                    httpPost.getParams().setParameter("jsonpost", jsonArray);

                    // Execute HTTP Post Request
                    System.out.print(jsonObject);
                    HttpResponse httpResponse= httpClient.execute(httpPost);
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }).start();

        if (response[0] != null) {
            return 1;
        } else {
            return 0;
        }
    }
    public static int LogExceptionError(String exception, String source, String username) {

        final ExceptionLog exceptionLog = new ExceptionLog();
        exceptionLog.setSource(source);
        exceptionLog.setUserName(username);
        exceptionLog.setDescription(exception);
        exceptionLog.setRefNumber(AndroidHelperUtility.generateRefNumber());

        final HttpResponse[] response = new HttpResponse[1];
        new Thread(new Runnable() {
            public void run() {
                // Create a new HttpClient and Post Header
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(AndroidHelperUtility.getBaseUrl("exceptionlog/create", AndroidHelperUtility.IS_DEBUG_MODE));
                try {

                    Gson gson = new Gson();
                    String jsonString = gson.toJson(exceptionLog);

                    JSONObject jsonObject = new JSONObject();
                    jsonObject = new JSONObject(jsonString);

                    JSONArray jsonArray = new JSONArray();
                    jsonArray.put(jsonObject);

                    // Post the data:
                    httpPost.setHeader("json", jsonObject.toString());
                    httpPost.getParams().setParameter("jsonpost", jsonArray);

                    // Execute HTTP Post Request
                    System.out.print(jsonObject);
                    HttpResponse httpResponse = httpClient.execute(httpPost);
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }).start();

        if (response[0] != null) {
            return 1;
        } else {
            return 0;
        }
    }

    public static int LogSystemError(Context context, final SystemLog systemLog) {

//        final HttpResponse[] response = new HttpResponse[1];
//        new Thread(new Runnable() {
//            public void run() {
//                // Create a new HttpClient and Post Header
//                HttpClient httpclient = new DefaultHttpClient();
//                HttpPost httppost = new HttpPost(AndroidHelperUtility.getBaseUrl("systemlog/create", true));
//                JSONObject jsonObject = new JSONObject();
//
//                try {
//                    Gson gson = new Gson();
//                    String jsonString = gson.toJson(systemLog);
//
//                    jsonObject = new JSONObject(jsonString);
//
//
//                    JSONArray jsonArray = new JSONArray();
//                    jsonArray.put(jsonObject);
//
//                    // Post the data:
//                    httppost.setHeader("json", jsonObject.toString());
//                    httppost.getParams().setParameter("jsonpost", jsonArray);
//
//                    // Execute HTTP Post Request
//                    System.out.print(jsonObject);
//
//                    response[0] = httpclient.execute(httppost);
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//        }).start();
        int timeout= 20 * 1000;
        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(timeout);

        Gson gson = new Gson();
        String jsonString = gson.toJson(systemLog);

        StringEntity entity = null;
        try {
            entity = new StringEntity(jsonString.toString());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
        String url = AndroidHelperUtility.getBaseUrl("systemlog/create", true);
        client.post(context, url, entity, "application/json", new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int i, Header[] headers, byte[] bytes) {
                Log.d(AndroidHelperUtility.STRING_LOG_TITLE_HTTP, "onSuccess: ");
            }

            @Override
            public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                Log.d(AndroidHelperUtility.STRING_LOG_TITLE_HTTP, "onFailure: ");
            }
        });
//        AsyncSystemLogCreatePost asyncSystemLog = new AsyncSystemLogCreatePost(systemLog);
//        JSONObject jsonObject = null;
//        try {
//            jsonObject = asyncSystemLog.execute().get();
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        } catch (ExecutionException e) {
//            e.printStackTrace();
//        }

//        if (jsonObject != null) {
//            return 1;
//        } else {
//            return 0;
//        }
        return 0;
    }
}

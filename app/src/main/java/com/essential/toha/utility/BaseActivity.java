package com.essential.toha.utility;

import java.util.HashMap;

import roboguice.activity.RoboActivity;

/**
 * Created by mtoha on 2/21/2016.
 */
public class BaseActivity extends RoboActivity {
    public String getUsernameLogin(){

        UserSessionManager session = new UserSessionManager(getApplicationContext());
        HashMap<String, String> user = session.getUserDetails();
        // get name
        String username = user.get(UserSessionManager.KEY_USERNAME);
        return  username;
    }
}

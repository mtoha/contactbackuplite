package com.essential.toha.model;

import java.util.List;

/**
 * Created by mtoha on 8/1/2015.
 */
public class Contact {
    private String name;
    private String username;
    private List< String> phoneNumber;
    private  List<String> email;

    public Contact() {
    }

    public Contact(String name, List<String> phoneNumber, List<String> email) {
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(List<String> phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public List<String> getEmail() {
        return email;
    }

    public void setEmail(List<String> email) {
        this.email = email;
    }
}

package com.essential.toha.model;

/**
 * Created by mtoha on 8/15/2015.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ExceptionLog {

    @Expose
    private int id = -1;
    @Expose
    private String refNumber = "-";
    @Expose
    private String source = "-";
    @Expose
    private String userName = "-";
    @Expose
    private String description = "-";

    public String getRefNumber() {
        return refNumber;
    }

    public void setRefNumber(String refNumber) {
        this.refNumber = refNumber;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}


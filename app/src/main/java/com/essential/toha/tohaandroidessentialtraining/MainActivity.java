package com.essential.toha.tohaandroidessentialtraining;

import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.Toast;

import com.essential.toha.adapter.ListViewAdapter;
import com.essential.toha.model.Contact;
import com.essential.toha.model.ExceptionLog;
import com.essential.toha.model.SystemLog;
import com.essential.toha.utility.AndroidHelperUtility;
import com.essential.toha.utility.BaseActivity;
import com.essential.toha.utility.UserSessionManager;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import roboguice.activity.RoboActivity;
import roboguice.inject.InjectView;


public class MainActivity extends BaseActivity {
    @InjectView(R.id.lvContact) ListView lvList;
    ListViewAdapter lvaContact;
    List<Contact> listContact = new ArrayList<Contact>();
    UserSessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        String sRefNumber = Long.toString(System.nanoTime());
        try {
            session = new UserSessionManager(getApplicationContext());
            if(session.checkLogin())
                finish();

            Toast.makeText(getApplicationContext(),
                    "Welcome " + getUsernameLogin() ,
                    Toast.LENGTH_LONG).show();

            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);

            fetchContacts();

            // Locate the ListView in listview_main.xml
            lvList = (ListView) findViewById(R.id.lvContact);

            // Pass results to ListViewAdapter Class
            lvaContact = new ListViewAdapter(this, R.layout.listview_item_contact,
                    listContact);

            // Binds the Adapter to the ListView
            lvList.setAdapter(lvaContact);
            lvList.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
            // Capture ListView item click
            lvList.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {

                @Override
                public void onItemCheckedStateChanged(ActionMode mode,
                                                      int position, long id, boolean checked) {
                    // Capture total checked items
                    final int checkedCount = lvList.getCheckedItemCount();
                    // Set the CAB title according to total checked items
                    mode.setTitle(checkedCount + " Selected");
                    // Calls toggleSelection method from ListViewAdapter Class
                    lvaContact.toggleSelection(position);
                }

                @Override
                public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.mnuDelete:
                            // Calls getSelectedIds method from ListViewAdapter Class
                            SparseBooleanArray selected = lvaContact
                                    .getSelectedIds();
                            // Captures all selected ids with a loop
                            for (int i = (selected.size() - 1); i >= 0; i--) {
                                if (selected.valueAt(i)) {
                                    Contact selectedContact = lvaContact
                                            .getItem(selected.keyAt(i));
                                    // Remove selected items following the ids
                                    lvaContact.remove(selectedContact);
                                }
                            }
                            // Close CAB
                            mode.finish();
                            return true;
                        default:
                            return false;
                    }
                }

                @Override
                public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                    mode.getMenuInflater().inflate(R.menu.menu_contact_list_action_mode, menu);
                    return true;
                }

                @Override
                public void onDestroyActionMode(ActionMode mode) {
                    lvaContact.removeSelection();
                }

                @Override
                public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                    return false;
                }
            });
        }catch (Exception ex){
            AndroidHelperUtility.LogExceptionError(ex.toString(),MainActivity.class.toString() ,getUsernameLogin() );
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        try {
            // Handle action bar item clicks here. The action bar will
            // automatically handle clicks on the Home/Up button, so long
            // as you specify a parent activity in AndroidManifest.xml.
            int id = item.getItemId();

            //noinspection SimplifiableIfStatement
            if (id == R.id.mnuSampleNotify) {
                AndroidHelperUtility.Notify(this, MainActivity.this, MainActivity.class, "Action Setting Selected", "Action Setting Selected Details");
            } else if (id == R.id.mnuLogout) {
                session.logoutUser();
            } else if (id == R.id.mnuStartBackup) {
                for (int i = 0; i < listContact.size(); i++) {
                    Contact contact = listContact.get(i);
                    AndroidHelperUtility.ContactCreate(contact);
                }
            } else if (id == R.id.mnuSamplePostException) {
                ExceptionLog exceptionLog = new ExceptionLog();
                exceptionLog.setSource(MainActivity.class.toString());
                exceptionLog.setUserName("mtoha");
                exceptionLog.setDescription("test");
                exceptionLog.setRefNumber(AndroidHelperUtility.generateRefNumber());

                AndroidHelperUtility.LogExceptionError(exceptionLog);
            } else if (id == R.id.mnuSamplePostSystemLog) {
                SystemLog systemLog = new SystemLog();
                systemLog.setRefNumber(AndroidHelperUtility.generateRefNumber());
                systemLog.setDescription("test");
                systemLog.setUserName("mtoha");
                systemLog.setSource(MainActivity.class.toString());

                AndroidHelperUtility.LogSystemError(getApplicationContext(), systemLog);
            } else if (id == R.id.mnuExit) {
                String sMessage = "Are You sure want to exit?";
                DialogInterface.OnClickListener onClickListenerYes = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        MainActivity.this.finish();
                    }
                };
                DialogInterface.OnClickListener onClickListenerNo = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                };
                AlertDialog alertDialog = AndroidHelperUtility.Alert(MainActivity.this, sMessage, "Yes", "No", onClickListenerYes, onClickListenerNo);
                alertDialog.show();
            }
        }catch (Exception ex){
            AndroidHelperUtility.LogExceptionError(ex.toString(),MainActivity.class.toString() ,getUsernameLogin() );
        }

        return super.onOptionsItemSelected(item);
    }


    public String fetchContacts() {

        String phoneNumber = null;
        String email = null;

        Uri CONTENT_URI = ContactsContract.Contacts.CONTENT_URI;
        String _ID = ContactsContract.Contacts._ID;
        String DISPLAY_NAME = ContactsContract.Contacts.DISPLAY_NAME;
        String HAS_PHONE_NUMBER = ContactsContract.Contacts.HAS_PHONE_NUMBER;

        Uri PhoneCONTENT_URI = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        String Phone_CONTACT_ID = ContactsContract.CommonDataKinds.Phone.CONTACT_ID;
        String NUMBER = ContactsContract.CommonDataKinds.Phone.NUMBER;

        Uri EmailCONTENT_URI = ContactsContract.CommonDataKinds.Email.CONTENT_URI;
        String EmailCONTACT_ID = ContactsContract.CommonDataKinds.Email.CONTACT_ID;
        String DATA = ContactsContract.CommonDataKinds.Email.DATA;

        StringBuffer output = new StringBuffer();

        ContentResolver contentResolver = getContentResolver();

        Cursor cursor = contentResolver.query(CONTENT_URI, null, null, null, null);
        // Loop for every contact in the phone
        if (cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                String contact_id = cursor.getString(cursor.getColumnIndex(_ID));
                String name = cursor.getString(cursor.getColumnIndex(DISPLAY_NAME));

                int hasPhoneNumber = Integer.parseInt(cursor.getString(cursor.getColumnIndex(HAS_PHONE_NUMBER)));

                if (hasPhoneNumber > 0) {


                    Contact contact = new Contact();
                    contact.setUsername(getUsernameLogin());
                    contact.setName(name);

                    output.append("\n First Name:" + name);

                    // Query and loop for every phone number of the contact
                    Cursor phoneCursor = contentResolver.query(PhoneCONTENT_URI, null, Phone_CONTACT_ID + " = ?", new String[]{contact_id}, null);
                    List<String> sPhoneNumbers = new ArrayList<String>();
                    while (phoneCursor.moveToNext()) {
                        phoneNumber = phoneCursor.getString(phoneCursor.getColumnIndex(NUMBER));
                        output.append("\n Phone number:" + phoneNumber);
                        sPhoneNumbers.add(phoneNumber);
                    }
                    phoneCursor.close();

                    // Query and loop for every email of the contact
                    Cursor emailCursor = contentResolver.query(EmailCONTENT_URI, null, EmailCONTACT_ID + " = ?", new String[]{contact_id}, null);
                    List<String> sEmails = new ArrayList<String>();
                    while (emailCursor.moveToNext()) {
                        email = emailCursor.getString(emailCursor.getColumnIndex(DATA));
                        output.append("\nEmail:" + email);
                        sEmails.add(email);
                    }
                    emailCursor.close();
                    contact.setPhoneNumber(sPhoneNumbers);
                    contact.setEmail(sEmails);
                    listContact.add(contact);
                }
                output.append("\n");
            }

        }
        return output.toString();

    }
}

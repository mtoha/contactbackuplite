package com.essential.toha.Async;

import android.os.AsyncTask;

import com.essential.toha.model.JSONParser;
import com.essential.toha.model.SystemLog;
import com.essential.toha.utility.AndroidHelperUtility;
import com.google.gson.Gson;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.ExecutionContext;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

/**
 * Created by mtoha on 8/15/2015.
 */

public class AsyncSystemLogCreatePost extends AsyncTask<String, String, JSONObject> {
    //    private ProgressDialog pDialog;
    private SystemLog systemLog;
    private HttpResponse httpResponse;
    private JSONObject jsonObjectResponse;
    public IAsyncResponse delegate = null;

    public AsyncSystemLogCreatePost(SystemLog systemLog) {
        this.systemLog = systemLog;
    }

    @Override
    protected void onPreExecute() {
//        super.onPreExecute();
//        uid = (TextView)findViewById(R.id.uid);
//        name1 = (TextView)findViewById(R.id.name);
//        email1 = (TextView)findViewById(R.id.email);
//        pDialog = new ProgressDialog(MainActivity.this);
//        pDialog.setMessage("Getting Data ...");
//        pDialog.setIndeterminate(false);
//        pDialog.setCancelable(true);
//        pDialog.show();

    }

    @Override
    protected JSONObject doInBackground(String... args) {
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost(AndroidHelperUtility.getBaseUrl("systemlog/create", true));

        JSONObject jsonObject = new JSONObject();

        try {
            Gson gson = new Gson();
            String jsonString = gson.toJson(systemLog);

            jsonObject = new JSONObject(jsonString);


            JSONArray jsonArray = new JSONArray();
            jsonArray.put(jsonObject);

            // Post the data:
            httppost.setHeader("json", jsonObject.toString());
            httppost.getParams().setParameter("jsonpost", jsonArray);

            // Execute HTTP Post Request
            System.out.print(jsonObject);

            httpResponse = httpclient.execute(httppost);

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    @Override
    protected void onPostExecute(JSONObject json) {
        //pDialog.dismiss();
        try {
            String response = null;
            response = EntityUtils.toString(httpResponse.getEntity());

            Gson gson = new Gson();
            systemLog = gson.fromJson(response, SystemLog.class);

            delegate.processFinish(json, httpResponse);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public HttpResponse getHttpResponse() {
        return httpResponse;
    }

    public void setHttpResponse(HttpResponse httpResponse) {
        this.httpResponse = httpResponse;
    }

    public JSONObject getJsonObjectResponse() {
        return jsonObjectResponse;
    }

    public void setJsonObjectResponse(JSONObject jsonObjectResponse) {
        this.jsonObjectResponse = jsonObjectResponse;
    }
}
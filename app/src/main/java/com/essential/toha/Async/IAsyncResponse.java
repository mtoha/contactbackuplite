package com.essential.toha.Async;

import org.apache.http.HttpResponse;
import org.json.JSONObject;

/**
 * Created by mtoha on 8/15/2015.
 */
public interface IAsyncResponse {
    void processFinish(JSONObject output,HttpResponse httpResponse);
}
package com.essential.toha.adapter;

/**
 * Created by mtoha on 8/1/2015.
 */


import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.essential.toha.model.Contact;
import com.essential.toha.tohaandroidessentialtraining.R;

public class ListViewAdapter extends ArrayAdapter<Contact> {
    // Declare Variables
    Context context;
    LayoutInflater inflater;
    List<Contact> contactList;
    private SparseBooleanArray mSelectedItemsIds;

    public ListViewAdapter(Context context, int resourceId,
                           List<Contact> contactList) {
        super(context, resourceId, contactList);
        mSelectedItemsIds = new SparseBooleanArray();
        this.context = context;
        this.contactList = contactList;
        inflater = LayoutInflater.from(context);
    }

    private class ViewHolder {
        TextView tvName;
        TextView tvPhone;
        TextView tvEmail;
        ImageView flag;
    }

    public View getView(int position, View view, ViewGroup parent) {
        final ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.listview_item_contact, null);
            // Locate the TextViews in listview_item.xml
            holder.tvName = (TextView) view.findViewById(R.id.txtName);
            holder.tvPhone = (TextView) view.findViewById(R.id.txtPhone);
            holder.tvEmail = (TextView) view.findViewById(R.id.txtEmail);
            // Locate the ImageView in listview_item.xml
            holder.flag = (ImageView) view.findViewById(R.id.flag);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        //Capture position and set to the TextViews
        holder.tvName.setText(contactList.get(position).getName());
        if (contactList.get(position).getPhoneNumber().size() > 0)
            holder.tvPhone.setText(contactList.get(position).getPhoneNumber().get(0));
        if (contactList.get(position).getEmail().size() > 0)
            holder.tvEmail.setText(contactList.get(position)
                    .getEmail().get(0));
        // Capture position and set to the ImageView
//        holder.flag.setImageResource(contactList.get(position)
//                .getFlag());
        return view;
    }

    @Override
    public void remove(Contact object) {
        contactList.remove(object);
        notifyDataSetChanged();
    }

    public List<Contact> getWorldPopulation() {
        return contactList;
    }

    public void toggleSelection(int position) {
        selectView(position, !mSelectedItemsIds.get(position));
    }

    public void removeSelection() {
        mSelectedItemsIds = new SparseBooleanArray();
        notifyDataSetChanged();
    }

    public void selectView(int position, boolean value) {
        if (value)
            mSelectedItemsIds.put(position, value);
        else
            mSelectedItemsIds.delete(position);
        notifyDataSetChanged();
    }

    public int getSelectedCount() {
        return mSelectedItemsIds.size();
    }

    public SparseBooleanArray getSelectedIds() {
        return mSelectedItemsIds;
    }
}
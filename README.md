# README #

Change http://192.168.42.109:8077 to the laravel url and port at AndroidHelperUtility

public static String getBaseUrl(String path, boolean debugMode) {
        String server = "http://192.168.42.109:8077";

### Client for ContactServer android based ###

* The program will backup and restore the user contact to cloud
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo) [Not Yet]

### How do I get set up? ###

* Libs : android-async-http-1.4.8.jar, gson-2.3.1.jar, guice-3.0-no_aop.jar, javax.inject-1.jar, jsr305-1.3.9.jar, roboguice-2.0.jar
* Change http://192.168.42.109:8077 to the laravel url and port at AndroidHelperUtility
* Database configuration at ContactServer\db_dump\contact_server.sql
* How to run tests : use ngrok.exe at cmd and run this command : ngrok.exe  http 8077
  copy result to AndroidHelperUtility.getBaseUrl for server url. Use Bluestack, emulator, etc which have internet connection to communicate with laravel. Real device is more prefered.
* Deployment instructions [Not Yet]

### Contribution guidelines ###

* Writing tests [Not Yet]
* Code review [Not Yet]
* Other guidelines [Not Yet]

### Who do I talk to? ###

* mudzakkirtoha@gmail.com or vair.agun@gmail.com
* Other community or team contact